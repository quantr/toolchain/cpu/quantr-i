/* verilator lint_off UNOPTFLAT */

module id(
    input wire rst,
    input wire[`PC_SIZE] pc_i,
    input wire[`InstBus] inst_i,
    input wire[`REG_SIZE] reg1_data_i,
    input wire[`REG_SIZE] reg2_data_i,
    input wire ex_wreg_i,
    input wire [`REG_SIZE] ex_wdata_i,
    input wire [`REG_ADDR_SIZE] ex_wd_i,
    input wire [`AluOpBus] ex_aluop_i,
    input wire [`PC_SIZE] ex_link_addr,
    input wire mem_wreg_i,
    input wire [`REG_SIZE] mem_wdata_i,
    input wire [`REG_ADDR_SIZE] mem_wd_i,
    input wire [`STALL_CYCLE] stall_cycle_i,

    output reg reg1_read_o,
    output reg reg2_read_o,
    output reg[`REG_ADDR_SIZE] reg1_addr_o,
    output reg[`REG_ADDR_SIZE] reg2_addr_o,
    output reg[`AluOpBus] aluop_o,
    output reg[`AluSelBus] alusel_o,
    output reg[`REG_SIZE] reg1_o,
    output reg[`REG_SIZE] reg2_o,
    output reg[`REG_ADDR_SIZE] wd_o,
    output reg wreg_o,
    output reg bubble,
    output reg stallreq,
    output reg [`STALL_CYCLE] stall_cycle,
    output reg [`MEM_ADDR_SIZE] mem_addr_o,
    output reg [`PC_SIZE] branch_target_address,
    output reg [`PC_SIZE] link_address,
    output reg need_flush_after_branch,
    output reg branch_flag
);

    //decode instruction
    wire[6:0] opcode = inst_i[6:0];
    wire[2:0] funct3 = inst_i[14:12];
    wire[6:0] funct7 = inst_i[31:25];
    wire[`REG_ADDR_SIZE] rd  = inst_i[11:7];
    wire[`REG_ADDR_SIZE] rs1 = inst_i[19:15];
    wire[`REG_ADDR_SIZE] rs2 = inst_i[24:20];
    wire[`PC_SIZE] next_pc;
    wire[`REG_SIZE] imm_sig_ext_12;
    wire[`REG_SIZE] branch_imm;

    reg [`REG_SIZE] imm;

    assign imm_sig_ext_12 = {{52{inst_i[31]}},inst_i[31:20]};
    assign branch_imm = {{51{inst_i[31]}},inst_i[31],inst_i[7],inst_i[30:25],inst_i[11:8],1'b0};
    assign next_pc = pc_i + 64'd4;

    //Load / branch dependency
    wire pre_inst_is_load;
    assign pre_inst_is_load = ((ex_aluop_i == `EXE_I_LB) || (ex_aluop_i == `EXE_I_LBU) || (ex_aluop_i == `EXE_I_LH) || (ex_aluop_i == `EXE_I_LHU) || (ex_aluop_i == `EXE_I_LW) || (ex_aluop_i == `EXE_I_LWU) || (ex_aluop_i == `EXE_I_LD)) ? 1'b1 : 1'b0;
    assign need_flush_after_branch = ((ex_aluop_i == `EXE_I_BEQ) || (ex_aluop_i == `EXE_I_BNE) || (ex_aluop_i == `EXE_I_BLT) || (ex_aluop_i == `EXE_I_BGE) || (ex_aluop_i == `EXE_I_BLTU) || (ex_aluop_i == `EXE_I_BGEU) || (ex_aluop_i == `EXE_I_JAL) || (ex_aluop_i == `EXE_I_JALR)) && (ex_link_addr == pc_i) ? 1'b1 : 1'b0;

    `define SET_INST(alusel, aluop, re1, re2, ra1, ra2, we, wa, imm_i) \
        alusel_o    = alusel; \
        aluop_o     = aluop; \
        reg1_read_o = re1; \
        reg2_read_o = re2; \
        reg1_addr_o = ra1; \
        reg2_addr_o = ra2; \
        wreg_o      = we; \
        wd_o        = wa; \
        imm         = imm_i;

    `define SET_BRANCH(branch, target, link) \
        branch_flag = branch; \
        branch_target_address = target; \
        link_address = link;

    `define BRANCH_CONDITION(condition) \
        if (condition == 1'd1) begin \
            `SET_BRANCH(1,(pc_i + branch_imm),next_pc) \
        end

    always @(*) begin
        if (rst == `RESET) begin
            `SET_INST(`EXE_RES_NOP,`EXE_NOP_OP,0,0,`ZeroRegAddr,`ZeroRegAddr,0,`ZeroRegAddr,`ZeroDWord)
            `STALL_SETTINGS(0,0,2'd0)
            `SET_BRANCH(`Disable,`ZeroDWord,`ZeroDWord)
            mem_addr_o = `ZeroMemAddr;
        end else begin
            `SET_INST(`EXE_RES_NOP,`EXE_NOP_OP,0,0,`ZeroRegAddr,`ZeroRegAddr,0,`ZeroRegAddr,`ZeroDWord)
            `STALL_SETTINGS(0,0,stall_cycle_i)
            `SET_BRANCH(`Disable,`ZeroDWord,`ZeroDWord)
            mem_addr_o = `ZeroMemAddr;
            if (opcode == 7'b0110111) begin
                //lui
                `SET_INST(`EXE_RES_LOGIC,`EXE_I_OR,0,0,rs1,rs2,1,rd,{{32{inst_i[31]}},inst_i[31:12],12'h000})
            end else if (opcode == 7'b0010111) begin
                //auipc
                `SET_INST(`EXE_RES_LOGIC,`EXE_I_OR,0,0,rs1,rs2,1,rd,({{32{inst_i[31]}},inst_i[31:12],12'h000} + pc_i))                        
            end else if (opcode == 7'b0110011) begin //Register to Register instructions
                if (funct3 == 3'b111 && funct7 == 7'd0) begin
                    //and
                    `SET_INST(`EXE_RES_LOGIC,`EXE_I_AND,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b110 && funct7 == 7'd0) begin
                    //or
                    `SET_INST(`EXE_RES_LOGIC,`EXE_I_OR,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b100 && funct7 == 7'd0) begin
                    //xor
                    `SET_INST(`EXE_RES_LOGIC,`EXE_I_XOR,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'd0 && funct7 == 7'd0) begin
                    //add
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_ADD,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'd0 && funct7 == 7'b0100000) begin
                    //sub
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_SUB,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b010 && funct7 == 7'd0) begin
                    //slt
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_SLT,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b011 && funct7 == 7'd0) begin
                    //sltu
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_SLTU,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b001 && funct7 == 7'd0) begin
                    //sll
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SLL,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b101 && funct7 == 7'd0) begin
                    //srl
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRL,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b101 && funct7 == 7'b0100000) begin
                    //sra
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRA,1,1,rs1,rs2,1,rd,0)
                end
            end else if (opcode == 7'b0010011) begin //Register to Imm instructions 
                if (funct3 == 3'b111) begin
                    //andi
                    `SET_INST(`EXE_RES_LOGIC,`EXE_I_AND,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b110) begin
                    //ori
                    `SET_INST(`EXE_RES_LOGIC,`EXE_I_OR,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b100) begin
                    //xori
                    `SET_INST(`EXE_RES_LOGIC,`EXE_I_XOR,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'd0) begin
                    //addi
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_ADD,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b010) begin
                    //slti
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_SLT,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b011) begin
                    //sltiu
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_SLTU,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b001 && funct7[6:1] == 6'd0) begin
                    //slli
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SLL,1,0,rs1,rs2,1,rd,{58'd0,inst_i[25:20]})
                end else if (funct3 == 3'b101 && funct7[6:1] == 6'd0) begin
                    //srli
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRL,1,0,rs1,rs2,1,rd,{58'd0,inst_i[25:20]})
                end else if (funct3 == 3'b101 && funct7[6:1] == 6'b010000) begin
                    //srai
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRA,1,0,rs1,rs2,1,rd,{58'd0,inst_i[25:20]})
                end
            end else if (opcode == 7'b0111011) begin //Register to Register W instructions
                if (funct3 == 3'd0 && funct7 == 7'd0) begin
                    //addw
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_ADDW,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'd0 && funct7 == 7'b0100000) begin
                    //subw
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_SUBW,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b001 && funct7 == 7'd0) begin
                    //sllw
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SLLW,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b101 && funct7 == 7'd0) begin
                    //srlw
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRLW,1,1,rs1,rs2,1,rd,0)
                end else if (funct3 == 3'b101 && funct7 == 7'b0100000) begin
                    //sraw
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRAW,1,1,rs1,rs2,1,rd,0)
                end 
            end else if (opcode == 7'b0011011) begin //Register to Imm W instructions
                if (funct3 == 3'd0) begin
                    //addiw
                    `SET_INST(`EXE_RES_ARITH,`EXE_I_ADDW,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b001 && funct7 == 7'd0) begin
                    //slliw
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SLLW,1,0,rs1,rs2,1,rd,{59'h0,inst_i[24:20]})
                end else if (funct3 == 3'b101 && funct7 == 7'd0) begin
                    //srliw
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRLW,1,0,rs1,rs2,1,rd,{59'h0,inst_i[24:20]})
                end else if (funct3 == 3'b101 && funct7 == 7'b0100000) begin
                    //sraiw
                    `SET_INST(`EXE_RES_SHIFT,`EXE_I_SRAW,1,0,rs1,rs2,1,rd,{59'h0,inst_i[24:20]})
                end
            end else if (opcode == 7'b1101111) begin
                //jal
                `SET_INST(`EXE_RES_JUMP,`EXE_I_JAL,0,0,rs1,rs2,1,rd,0)
                `SET_BRANCH(1,(pc_i+ {{44{inst_i[31]}},inst_i[19:12],inst_i[20],inst_i[30:21], 1'b0}),next_pc)
            end else if (opcode == 7'b1100111) begin
                //jalr
                `SET_INST(`EXE_RES_JUMP,`EXE_I_JALR,1,0,rs1,rs2,1,rd,0)
                `SET_BRANCH(1,(reg1_o + imm_sig_ext_12),next_pc)
            end else if (opcode == 7'b1100011) begin //Branch instructions
                if (funct3 == 3'b000) begin
                    //beq
                    `SET_INST(`EXE_RES_JUMP,`EXE_I_BEQ,1,1,rs1,rs2,0,rd,0)
                    `BRANCH_CONDITION(reg1_o == reg2_o)
                    // if (reg1_o == reg2_o) `SET_BRANCH(1,(pc_i + branch_imm),next_pc)
                end else if (funct3 == 3'b001) begin
                    //bne
                    `SET_INST(`EXE_RES_JUMP,`EXE_I_BNE,1,1,rs1,rs2,0,rd,0)
                    `BRANCH_CONDITION(reg1_o != reg2_o)
                    // if (reg1_o != reg2_o) begin `SET_BRANCH(1,(pc_i + branch_imm),next_pc) end
                end else if (funct3 == 3'b100) begin
                    //blt
                    `SET_INST(`EXE_RES_JUMP,`EXE_I_BLT,1,1,rs1,rs2,0,rd,0)
                    `BRANCH_CONDITION((reg1_o[63] && !reg2_o[63]) || (!reg1_o[63] && !reg2_o[63] && (reg1_o[62:0] < reg2_o[62:0])) || (reg1_o[63] && reg2_o[63] && (reg1_o[62:0] < reg2_o[62:0])))
                    // if ((reg1_o[63] && !reg2_o[63]) || (!reg1_o[63] && !reg2_o[63] && (reg1_o[62:0] < reg2_o[62:0])) || (reg1_o[63] && reg2_o[63] && (reg1_o[62:0] < reg2_o[62:0]))) `SET_BRANCH(1,(pc_i + branch_imm),next_pc)
                end else if (funct3 == 3'b101) begin
                    //bge
                    `SET_INST(`EXE_RES_JUMP,`EXE_I_BGE,1,1,rs1,rs2,0,rd,0)
                    `BRANCH_CONDITION((!reg1_o[63] && reg2_o[63]) || (!reg1_o[63] && !reg2_o[63] && (reg1_o[62:0] > reg2_o[62:0])) || (reg1_o[63] && reg2_o[63] && (reg1_o[62:0] > reg2_o[62:0])))
                    // if ((!reg1_o[63] && reg2_o[63]) || (!reg1_o[63] && !reg2_o[63] && (reg1_o[62:0] > reg2_o[62:0])) || (reg1_o[63] && reg2_o[63] && (reg1_o[62:0] > reg2_o[62:0]))) `SET_BRANCH(1,(pc_i + branch_imm),next_pc)
                end else if (funct3 == 3'b110) begin
                    //bltu
                    `SET_INST(`EXE_RES_JUMP,`EXE_I_BLTU,1,1,rs1,rs2,0,rd,0)
                    `BRANCH_CONDITION(reg1_o < reg2_o)
                    // if (reg1_o < reg2_o) `SET_BRANCH(1,(pc_i + branch_imm),next_pc)
                end else if (funct3 == 3'b111) begin
                    //bgeu
                    `SET_INST(`EXE_RES_JUMP,`EXE_I_BGEU,1,1,rs1,rs2,0,rd,0)
                    `BRANCH_CONDITION(reg1_o > reg2_o)
                    // if (reg1_o > reg2_o) `SET_BRANCH(1,(pc_i + branch_imm),next_pc)
                end 
            end else if (opcode == 7'b0000011) begin //Load Instructions
                mem_addr_o = (reg1_o[38:0] + {{26{inst_i[31]}},inst_i[31:20]}) & 39'h7fffffffff;
                if (funct3 == 3'b000) begin
                    //lb
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LB,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b001) begin
                    //lh
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LH,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b010) begin
                    //lw
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LW,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b100) begin
                    //lbu
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LBU,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b101) begin
                    //lhu
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LHU,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b110) begin
                    //lwu
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LWU,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end else if (funct3 == 3'b011) begin
                    //ld
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_LD,1,0,rs1,rs2,1,rd,imm_sig_ext_12)
                end 
            end else if (opcode == 7'b0100011) begin //Store Instuctions
                mem_addr_o = (reg1_o[38:0] + {{26{inst_i[31]}},inst_i[31:25],inst_i[11:7]}) & 39'h7fffffffff; //Memory address for store instructions under SV39 
                if (funct3 == 3'b000) begin
                    //sb
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_SB,1,1,rs1,rs2,0,rd,0)
                end else if (funct3 == 3'b001) begin
                    //sh
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_SH,1,1,rs1,rs2,0,rd,0)
                end else if (funct3 == 3'b010) begin
                    //sw
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_SW,1,1,rs1,rs2,0,rd,0)
                end else if (funct3 == 3'b011) begin
                    //sd
                    `SET_INST(`EXE_RES_LD_STR,`EXE_I_SD,1,1,rs1,rs2,0,rd,0)
                end 
            end else if (opcode == 7'b1110011 && rd == `ZeroRegAddr && rs1 == `ZeroRegAddr && funct3 == 3'd0 && funct7 == 7'd0) begin //ECALL & EBREAK
                if (rs2 == 5'd0) begin
                    //ECALL
                    `SET_INST(`EXE_RES_SYSTEM,`EXE_I_ECALL,0,0,rs1,rs2,0,rd,0)
                end else if (rs2 == 5'd1) begin
                    //EBREAK
                    `SET_INST(`EXE_RES_SYSTEM,`EXE_I_EBREAK,0,0,rs1,rs2,0,rd,0)
                end
            end else if (opcode == 7'b0001111) begin
                //FENCE
                `SET_INST(`EXE_RES_SYSTEM,`EXE_I_FENCE,1,0,rs1,rs2,1,rd,imm)
            end
            if (pre_inst_is_load == `Enable && (ex_wd_i == reg1_addr_o || ex_wd_i == reg2_addr_o) && (reg1_read_o == `Enable || reg2_read_o == `Enable)) begin
                stallreq = `Enable;
            end
        end
    end
    
    always @(*) begin
        if (rst == `RESET) begin
            reg1_o=`ZeroDWord;
        end else if((reg1_read_o == `Enable) && (ex_wreg_i == `Enable) && (ex_wd_i == reg1_addr_o)) begin
            reg1_o=ex_wdata_i;
        end else if((reg1_read_o == `Enable) && (mem_wreg_i == `Enable) && (mem_wd_i == reg1_addr_o)) begin
            reg1_o=mem_wdata_i;
        end else if (reg1_read_o == `Enable) begin
            reg1_o=reg1_data_i;
        end else if (reg1_read_o == `Disable) begin
            reg1_o = imm;
        end else begin
            reg1_o=`ZeroDWord;
        end
    end
    
    always @(*) begin
        if (rst == `RESET) begin
            reg2_o=`ZeroDWord;
        end else if((reg2_read_o == `Enable) && (ex_wreg_i == `Enable) && (ex_wd_i == reg2_addr_o)) begin
            reg2_o=ex_wdata_i;
        end else if((reg2_read_o == `Enable) && (mem_wreg_i == `Enable) && (mem_wd_i == reg2_addr_o)) begin
            reg2_o=mem_wdata_i;
        end else if (reg2_read_o == `Enable) begin
            reg2_o=reg2_data_i;
        end else if (reg2_read_o == `Disable) begin
            reg2_o = imm;
        end else begin
            reg2_o=`ZeroDWord;
        end
    end

endmodule
