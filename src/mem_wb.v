/* verilator lint_off UNUSED */
module mem_wb (
    input wire clk, 
    input wire rst,
    input wire [`REG_ADDR_SIZE] mem_wd,
    input wire mem_wreg,
    input wire [`REG_SIZE] mem_wdata,
    input wire [`STALL_SIZE] stall,
    input wire [`STALL_CYCLE] stall_cycle_i,
    input wire bubble,
   
    output reg [`REG_ADDR_SIZE] wb_wd,
    output reg wb_wreg,
    output reg [`REG_SIZE] wb_wdata,
    output reg [`STALL_CYCLE] stall_cycle
);
    `define MEMWB_SETTINGS(we, wa, wdata, cycle) \
        wb_wreg     <= we; \
        wb_wd       <= wa; \
        wb_wdata    <= wdata; \
        stall_cycle <= cycle;

    always @(posedge clk) begin
        if(rst == `RESET) begin
            `MEMWB_SETTINGS(`Disable,`ZeroRegAddr,`ZeroDWord,`ZeroCycle)
        end else if(stall[4] == `Enable && stall[5] == `Disable && bubble == `Disable) begin
            `MEMWB_SETTINGS(`Disable,`ZeroRegAddr,`ZeroDWord,stall_cycle_i)
        end else if(stall[4] == `Enable && stall[5] == `Disable && bubble == `Enable && stall_cycle_i == 2'd1) begin
            `MEMWB_SETTINGS(mem_wreg,mem_wd,mem_wdata,stall_cycle_i)
        end else if(stall[4] == `Disable) begin
            `MEMWB_SETTINGS(mem_wreg,mem_wd,mem_wdata,`ZeroCycle)
        end else begin
            stall_cycle <= stall_cycle_i;
        end
    end
    
endmodule
