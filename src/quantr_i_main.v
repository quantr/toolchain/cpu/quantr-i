`include "constant.v"
`include "quantr_dpi.h"

module quantr_i_main (
    input wire clk,
    input wire rst
);

    wire [`PC_SIZE] inst_addr; //Return value of quantr_i pc address -> Rom
    wire [`InstBus] inst; //The instruction from Rom -> quantr_i
    wire rom_ce;
    wire [`REG_SIZE] data_to_quantr;
    wire [`REG_SIZE] data_to_ram;
    wire [`MEM_ADDR_SIZE] addr_to_ram;
    wire [7:0] sel_to_ram;
    wire we_to_ram;
    wire ce_to_ram;

    quantr_i quantr_i(
        .clk(clk),
        .rst(rst),
        .rom_data_i(inst),
        .ram_data_i(data_to_quantr),

        .rom_addr_o(inst_addr),
        .rom_ce_o(rom_ce),

        .ram_data_o(data_to_ram),
        .ram_addr_o(addr_to_ram),
        .ram_sel_o(sel_to_ram),
        .ram_we_o(we_to_ram),
        .ram_ce_o(ce_to_ram)
    );

    rom rom(
        .ce(rom_ce),
        .addr(inst_addr),

        .inst(inst)
    );

    data_ram data_ram(
        .clk(clk),
        .data_i(data_to_ram),
        .addr(addr_to_ram),
        .sel(sel_to_ram),
        .we(we_to_ram),
        .ce(ce_to_ram),

        .data_o(data_to_quantr)
    );
endmodule
