`ifndef CONSTANT_V
`define CONSTANT_V

`define MXLEN           63:0
`define Word            31:0
`define Halfword        15:0
`define Byte            7:0

// Source Variables
`define InstBus         31:0        // Size of Inst, Used for storing inst
`define PC_SIZE         63:0        // Used for storing Inst Address, As PC counter
`define REG_SIZE        63:0        // Size of Register
`define MEM_ADDR_SIZE   38:0        // Size of Memory Address, SV39 memory system
`define STALL_SIZE      5:0         // Size of Ctrl module stall 
`define STALL_CYCLE     1:0         // allow to stop pipeline for max = 4 clk
`define PC_StartPoint   64'h0000000000400000  // Default Start Point of PC 
`define REG_ADDR_SIZE   4:0         // Used for storing the address of register x0 to x31
`define AluOpBus        15:0        // Define Instruction
`define AluSelBus       2:0         // Define Type
`define InstBusWidth  	20
`define DataBusWidth  	28

// `define DataMemNum 		549755813888
// `define DataMemNumLog2	39

// Clear Values
`define ZeroRegAddr     5'd0
`define ZeroWord        32'd0
`define ZeroDWord       64'd0
`define ZeroMemAddr     39'd0
`define ZeroCycle		2'd0

// Set or Reset
`define RESET           1'b1
`define Enable          1'b1
`define Disable         1'b0

/*opcode total in 16 bit
 15:12 bit is used to define type, 
 eg. 0 (32i + 64i), 1 (32m + 64m), 2 (32a+64a), 3 (32c + 64c)
 11:0 bit is used to define inst
*/

/* RV64I instructions (32i + 64i) */
`define EXE_NOP_OP      16'b0000

//Logical
`define EXE_I_AND       16'h0001
`define EXE_I_OR        16'h0002
`define EXE_I_XOR       16'h0003

//Arithmetic
`define EXE_I_ADD       16'h0004
`define EXE_I_SUB       16'h0005
`define EXE_I_SLT       16'h0006
`define EXE_I_SLTU      16'h0007
`define EXE_I_ADDW      16'h0008
`define EXE_I_SUBW      16'h0009

//Shift
`define EXE_I_SLL       16'h000a
`define EXE_I_SRL       16'h000b
`define EXE_I_SRA       16'h000c
`define EXE_I_SLLW      16'h000d
`define EXE_I_SRLW      16'h000e
`define EXE_I_SRAW      16'h000f

//Jump Branch
`define EXE_I_JAL       16'h0010
`define EXE_I_JALR      16'h0011
`define EXE_I_BEQ       16'h0012
`define EXE_I_BNE       16'h0013
`define EXE_I_BLT       16'h0014
`define EXE_I_BGE       16'h0015
`define EXE_I_BLTU      16'h0016
`define EXE_I_BGEU      16'h0017               

//Load
`define EXE_I_LB        16'h0018
`define EXE_I_LH        16'h0019
`define EXE_I_LW        16'h001a
`define EXE_I_LBU       16'h001b
`define EXE_I_LHU       16'h001c
`define EXE_I_LWU       16'h001d
`define EXE_I_LD        16'h001e

//Store
`define EXE_I_SB        16'h001f
`define EXE_I_SH        16'h0020
`define EXE_I_SW        16'h0021
`define EXE_I_SD        16'h0022

//Special
`define EXE_I_FENCE     16'h0023
`define EXE_I_ECALL     16'h0024
`define EXE_I_EBREAK    16'h0025

//AluSel
`define EXE_RES_NOP     3'b000
`define EXE_RES_LOGIC   3'b001
`define EXE_RES_SHIFT   3'b010
`define EXE_RES_ARITH   3'b011
`define EXE_RES_JUMP    3'b100
`define EXE_RES_LD_STR  3'b101
`define EXE_RES_SYSTEM  3'b111

`define STALL_SETTINGS(stop, bubble_i, cycle) \
	stall_cycle = cycle; \
	stallreq 	= stop; \
	bubble 		= bubble_i;

//Example Usage: " `SET_STALL(2'd2, `Enable) " (meaning: stop pipeline for 2 clocks, inserting bubble)
`define SET_STALL(clks_to_stop, insert_bubble) \
	if (stall_cycle < clks_to_stop) begin \
		stallreq = `Enable; \
		stall_cycle = stall_cycle_i + 1; \
		if (insert_bubble == `Enable) begin \
			bubble = `Enable; \
		end \
	end else if (stall_cycle == clks_to_stop) begin \
		`STALL_SETTINGS(0,0,2'd0) \
	end

`endif
