/* verilator lint_off UNOPTFLAT */

module mem (
    input wire rst,
    input wire [`REG_ADDR_SIZE] wd_i,
    input wire wreg_i,
    input wire [`REG_SIZE] wdata_i,
    
    input wire [`AluOpBus] aluop_i,
    input wire [`REG_SIZE] reg2_i,
    input wire [`MEM_ADDR_SIZE] mem_addr_i,
    input wire [`REG_SIZE] mem_data_i,
    
    input wire [`STALL_CYCLE] stall_cycle_i,

    output reg [`REG_ADDR_SIZE] wd_o,
    output reg wreg_o,
    output reg [`REG_SIZE] wdata_o,
    
    output reg [`MEM_ADDR_SIZE] mem_addr_o,
    output reg [`REG_SIZE] mem_data_o,
    output reg [7:0] mem_sel_o,
    output wire mem_we_o,
    output reg mem_ce_o,
    
    output reg bubble,
    output reg stallreq,
    output reg [1:0] stall_cycle
);
    
    reg mem_we;
    assign mem_we_o = mem_we;

    `define MEM_SETTINGS(we, wa, wdata) \
        wreg_o  = we; \
        wd_o    = wa; \
        wdata_o = wdata;

    `define SET_MEM(memaddr, memwe, memwdata, memce, memsel) \
        mem_addr_o = memaddr; \
        mem_we = memwe; \
        mem_data_o = memwdata; \
        mem_sel_o = memsel; \
        mem_ce_o = memce;

    always @(*) begin
        if (rst == `RESET) begin
            `MEM_SETTINGS(`Disable,`ZeroRegAddr,`ZeroDWord)
            `SET_MEM(`ZeroMemAddr,`Disable,`ZeroDWord,`Disable,8'd0)
            `STALL_SETTINGS(0,0,2'd0)
        end else begin
            `MEM_SETTINGS(wreg_i,wd_i,wdata_i)
            `SET_MEM(`ZeroMemAddr,`Disable,`ZeroDWord,`Disable,8'd0)
            `STALL_SETTINGS(0,0,stall_cycle_i)
            case(aluop_i)
                `EXE_I_LB : begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = {{56{mem_data_i[7]}}, mem_data_i[7:0]};
                end
                `EXE_I_LBU: begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = {56'd0, mem_data_i[7:0]};
                end
                `EXE_I_LH : begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = {{48{mem_data_i[15]}}, mem_data_i[15:0]};
                end
                `EXE_I_LHU : begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = {48'd0, mem_data_i[15:0]};
                end
                `EXE_I_LW : begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = {{32{mem_data_i[31]}}, mem_data_i[31:0]};
                end
                `EXE_I_LWU : begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = {32'd0, mem_data_i[31:0]};
                end
                `EXE_I_LD : begin
                    `SET_MEM(mem_addr_i,`Disable,`ZeroDWord,`Enable,8'd0)
                    wdata_o = mem_data_i;
                end
                `EXE_I_SB : begin
                    `SET_MEM(mem_addr_i,`Enable,{56'd0,reg2_i[7:0]},`Enable,8'd1)
                end
                `EXE_I_SH : begin
                    `SET_MEM(mem_addr_i,`Enable,{48'd0,reg2_i[15:0]},`Enable,8'b00000011)
                end
                `EXE_I_SW : begin
                    `SET_MEM(mem_addr_i,`Enable,{32'd0,reg2_i[31:0]},`Enable,8'b00001111)                    
                end
                `EXE_I_SD : begin
                    `SET_MEM(mem_addr_i,`Enable,reg2_i,`Enable,8'b11111111)                                        
                end
                default: begin
                    //Do nothing
                end
            endcase
        end
    end
endmodule
