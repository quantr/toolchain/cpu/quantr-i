/* verilator lint_off UNOPTFLAT */

`include "constant.v"
module quantr_i(
    input wire clk,
    input wire rst,
    input wire [`InstBus] rom_data_i,
    input wire [`REG_SIZE] ram_data_i,

    output wire [`PC_SIZE] rom_addr_o,
    output wire rom_ce_o,

    output wire [`REG_SIZE] ram_data_o,
    output wire [`MEM_ADDR_SIZE] ram_addr_o,
    output wire [7:0] ram_sel_o,
    output wire ram_we_o,
    output wire ram_ce_o
);
    
    //Connect PC
    wire[`PC_SIZE] pc;
    wire[`PC_SIZE] id_branchtarget_pc;
    wire id_branchflag_pc;
    
    //Connect IF/ID and ID
    wire[`PC_SIZE] ifid_pc_id;
    wire[`InstBus] ifid_inst_id;

    //Connect between Register and ID
    wire[`REG_SIZE] reg_reg1_data_id;
    wire[`REG_SIZE] reg_reg2_data_id;
    wire id_reg1_read_o_reg;
    wire id_reg2_read_o_reg;
    wire[`REG_ADDR_SIZE] id_reg1_addr_o_reg;
    wire[`REG_ADDR_SIZE] id_reg2_addr_o_reg;

    //Connect ID and ID/EX
    wire[`AluOpBus] id_aluop_idex;
    wire[`AluSelBus] id_alusel_idex;
    wire[`REG_SIZE] id_reg1_idex;
    wire[`REG_SIZE] id_reg2_idex;
    wire[`REG_ADDR_SIZE] id_wd_idex;
    wire id_wreg_idex;
    wire [`MEM_ADDR_SIZE] id_memaddr_idex;
    wire [`PC_SIZE] id_linkaddr_idex;
    wire id_prebranch_idex;

    //Connect ID/EX to EX
    wire [`AluOpBus] idex_aluop_ex;
    wire [`AluSelBus] idex_aluse1_ex;
    wire [`REG_SIZE] idex_reg1_ex;
    wire [`REG_SIZE] idex_reg2_ex;
    wire [`REG_ADDR_SIZE] idex_wd_ex;
    wire idex_wreg_ex;
    wire [`MEM_ADDR_SIZE] idex_memaddr_ex;
    wire [`PC_SIZE] idex_linkaddress_ex;

    //Connect EX to EX/MEM
    wire [`REG_ADDR_SIZE] ex_wd_exmem;
    wire ex_wreg_exmem;
    wire [`REG_SIZE] ex_wdata_exmem;
    wire [`AluOpBus] ex_aluop_exmem;
    wire [`MEM_ADDR_SIZE] ex_memaddr_exmem;
    wire [`REG_SIZE] ex_reg2_exmem;

    //Connect EX/MEM to MEM
    wire [`REG_ADDR_SIZE] exmem_wd_mem;
    wire exmem_wreg_mem;
    wire [`REG_SIZE] exmem_wdata_mem;
    wire [`AluOpBus] exmem_aluop_mem;
    wire [`REG_SIZE] exmem_reg2_mem;
    wire [`MEM_ADDR_SIZE] exmem_memaddr_mem;

    //Connect MEM to MEM/WB
    wire [`REG_ADDR_SIZE] mem_wd_memwb;
    wire mem_wreg_memwb;
    wire [`REG_SIZE] mem_wdata_memwb;

    //Connect MEM/WB to Register
    wire [`REG_ADDR_SIZE] wb_wd_reg;
    wire wb_wreg_reg;
    wire [`REG_SIZE] wb_wdata_reg;

    //Connect Ctrl with others 
    wire ctrl_id;
    wire ctrl_ex;
    wire ctrl_mem;
    wire id_bubble;
    wire ex_bubble;
    wire mem_bubble;
    wire [`STALL_SIZE] ctrl_stall;
    wire [`STALL_CYCLE] id_stall_cycle_idex;
    wire [`STALL_CYCLE] idex_stall_cycle_id;
    wire [`STALL_CYCLE] ex_stall_cycle_exmem;
    wire [`STALL_CYCLE] exmem_stall_cycle_ex;
    wire [`STALL_CYCLE] mem_stall_cycle_memwb;
    wire [`STALL_CYCLE] memwb_stall_cycle_mem;

    pc_reg pc_reg(
    .clk(clk),
    .rst(rst),
    .stall(ctrl_stall),
    .branch_flag(id_branchflag_pc),
    .branch_target_address(id_branchtarget_pc),

    .pc(pc),
    .ce(rom_ce_o)
    );
    
    assign rom_addr_o = pc;

    if_id if_id0(
    .clk(clk),
    .rst(rst),
    .if_pc(pc),
    .if_inst(rom_data_i),
    .stall(ctrl_stall),

    .id_pc(ifid_pc_id),
    .id_inst(ifid_inst_id)
    );

    register register0(
    .clk(clk),
    .rst(rst),
    .we(wb_wreg_reg),
    .waddr(wb_wd_reg),
    .wdata(wb_wdata_reg),
    .re1(id_reg1_read_o_reg),
    .raddr1(id_reg1_addr_o_reg),
    .re2(id_reg2_read_o_reg),
    .raddr2(id_reg2_addr_o_reg),
    
    .rdata1(reg_reg1_data_id),
    .rdata2(reg_reg2_data_id)
    );

    id id0(
    .rst(rst),
    .pc_i(ifid_pc_id),
    .inst_i(ifid_inst_id),
    .reg1_data_i(reg_reg1_data_id),
    .reg2_data_i(reg_reg2_data_id),
    .ex_wreg_i(ex_wreg_exmem),
    .ex_wdata_i(ex_wdata_exmem),
    .ex_wd_i(ex_wd_exmem),
    .ex_aluop_i(ex_aluop_exmem),
    .ex_link_addr(idex_linkaddress_ex),
    .mem_wreg_i(mem_wreg_memwb),
    .mem_wdata_i(mem_wdata_memwb),
    .mem_wd_i(mem_wd_memwb),
    .stall_cycle_i(idex_stall_cycle_id),
    
    .reg1_read_o(id_reg1_read_o_reg),
    .reg2_read_o(id_reg2_read_o_reg),
    .reg1_addr_o(id_reg1_addr_o_reg),
    .reg2_addr_o(id_reg2_addr_o_reg),
    .aluop_o(id_aluop_idex),
    .alusel_o(id_alusel_idex),
    .reg1_o(id_reg1_idex),
    .reg2_o(id_reg2_idex),
    .wd_o(id_wd_idex),
    .wreg_o(id_wreg_idex),
    .bubble(id_bubble),
    .stallreq(ctrl_id),
    .stall_cycle(id_stall_cycle_idex),
    .mem_addr_o(id_memaddr_idex),
    .branch_target_address(id_branchtarget_pc),
    .link_address(id_linkaddr_idex),
    .need_flush_after_branch(id_prebranch_idex),
    .branch_flag(id_branchflag_pc)
    );

    id_ex id_ex0(
    .clk(clk),
    .rst(rst),
    .id_aluop_i(id_aluop_idex),
    .id_alusel_i(id_alusel_idex),
    .id_reg1_i(id_reg1_idex),
    .id_reg2_i(id_reg2_idex),
    .id_wd_i(id_wd_idex),
    .id_wreg_i(id_wreg_idex),
    .stall(ctrl_stall),
    .stall_cycle_i(id_stall_cycle_idex),
    .bubble(id_bubble),
    .mem_addr_i(id_memaddr_idex),
    .link_address(id_linkaddr_idex),
    .need_flush_after_branch(id_prebranch_idex),

    .ex_aluop_o(idex_aluop_ex),
    .ex_alusel_o(idex_aluse1_ex),
    .ex_reg1_o(idex_reg1_ex),
    .ex_reg2_o(idex_reg2_ex),
    .ex_wd_o(idex_wd_ex),
    .ex_wreg_o(idex_wreg_ex),
    .stall_cycle(idex_stall_cycle_id),
    .mem_addr_o(idex_memaddr_ex),
    .link_address_o(idex_linkaddress_ex)
    );

    ex ex0(
    .rst(rst),
    .aluop_i(idex_aluop_ex),
    .aluse_i(idex_aluse1_ex),
    .reg1_i(idex_reg1_ex),
    .reg2_i(idex_reg2_ex),
    .wd_i(idex_wd_ex),
    .wreg_i(idex_wreg_ex),
    .stall_cycle_i(exmem_stall_cycle_ex),
    .mem_addr_i(idex_memaddr_ex),
    .link_address(idex_linkaddress_ex),

    .wd_o(ex_wd_exmem),
    .wreg_o(ex_wreg_exmem),
    .wdata_o(ex_wdata_exmem),
    .bubble(ex_bubble),
    .stallreq(ctrl_ex),
    .stall_cycle(ex_stall_cycle_exmem),
    .aluop_o(ex_aluop_exmem),
    .mem_addr_o(ex_memaddr_exmem),
    .reg2_o(ex_reg2_exmem)
    );
    
    ex_mem ex_mem0(
    .clk(clk),
    .rst(rst),
    .ex_wd(ex_wd_exmem),
    .ex_wreg(ex_wreg_exmem),
    .ex_wdata(ex_wdata_exmem),
    .ex_aluop(ex_aluop_exmem),
    .ex_reg2(ex_reg2_exmem),
    .mem_addr_i(ex_memaddr_exmem),
    .stall(ctrl_stall),
    .stall_cycle_i(ex_stall_cycle_exmem),
    .bubble(ex_bubble),

    .mem_wd(exmem_wd_mem),
    .mem_wreg(exmem_wreg_mem),
    .mem_wdata(exmem_wdata_mem),
    .mem_aluop(exmem_aluop_mem),
    .mem_reg2(exmem_reg2_mem),
    .stall_cycle(exmem_stall_cycle_ex),
    .mem_addr_o(exmem_memaddr_mem)
    );

    mem mem0(
    .rst(rst),
    .wd_i(exmem_wd_mem),
    .wreg_i(exmem_wreg_mem),
    .wdata_i(exmem_wdata_mem),
    .aluop_i(exmem_aluop_mem),
    .reg2_i(exmem_reg2_mem),
    .mem_addr_i(exmem_memaddr_mem),
    .mem_data_i(ram_data_i),
    .stall_cycle_i(memwb_stall_cycle_mem),

    .wd_o(mem_wd_memwb),
    .wreg_o(mem_wreg_memwb),
    .wdata_o(mem_wdata_memwb),

    .mem_addr_o(ram_addr_o),
    .mem_data_o(ram_data_o),
    .mem_sel_o(ram_sel_o),
    .mem_we_o(ram_we_o),
    .mem_ce_o(ram_ce_o),
    .bubble(mem_bubble),
    .stallreq(ctrl_mem),
    .stall_cycle(mem_stall_cycle_memwb)
    );
    
    mem_wb mem_wb0(
    .clk(clk),
    .rst(rst),
    .mem_wd(mem_wd_memwb),
    .mem_wreg(mem_wreg_memwb),
    .mem_wdata(mem_wdata_memwb),
    .stall(ctrl_stall),
    .stall_cycle_i(mem_stall_cycle_memwb),
    .bubble(mem_bubble),

    .wb_wd(wb_wd_reg),
    .wb_wreg(wb_wreg_reg),
    .wb_wdata(wb_wdata_reg),
    .stall_cycle(memwb_stall_cycle_mem)
    );

    ctrl ctrl0(
    .rst(rst),
    .stallreq_id(ctrl_id),
    .stallreq_ex(ctrl_ex),
    .stallreq_mem(ctrl_mem),

    .stall(ctrl_stall)
    );

endmodule
