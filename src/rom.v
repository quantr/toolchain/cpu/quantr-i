/* verilator lint_off UNOPTFLAT */
/* verilator lint_off UNUSED */

module rom (
    input wire ce, 
    input wire [`PC_SIZE] addr,
    output reg [`InstBus] inst
);

    reg [`Word] inst_mem[(1<<`InstBusWidth)-1:0];

    initial $readmemh("rom.data", inst_mem);

    always @(*) begin
        if(ce == `Disable) begin
            inst = `ZeroWord;
        end else begin
            inst = inst_mem[addr[`InstBusWidth + 1 : 2]];
        end
    end
    
endmodule
