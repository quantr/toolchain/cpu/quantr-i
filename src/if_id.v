/* verilator lint_off UNUSED */
module if_id(
    input wire clk,
    input wire rst,
    input wire[`PC_SIZE] if_pc,
    input wire [`InstBus] if_inst,
    input wire [`STALL_SIZE] stall,

    output reg[`PC_SIZE] id_pc,
    output reg[`InstBus] id_inst
);

    always @(posedge clk) begin
        if (rst == `RESET) begin
            id_pc   <= `ZeroDWord;
            id_inst <= `ZeroWord;
        end else if (stall[1] == `Enable && stall[2] == `Disable) begin
            id_pc   <= `ZeroDWord;
            id_inst <= `ZeroWord; 
        end else if (stall[1] == `Disable) begin
            id_pc   <= if_pc;
            id_inst <= if_inst;
        end
    end
endmodule
