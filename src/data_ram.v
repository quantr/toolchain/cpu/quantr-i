/* verilator lint_off UNOPTFLAT */
/* verilator lint_off UNUSED */

module data_ram (
    input wire clk,
    input wire ce,
    input wire we,
    input wire [`MEM_ADDR_SIZE] addr,
    input wire [7:0] sel,
    input wire [`REG_SIZE] data_i,
    
    output reg [`REG_SIZE] data_o
);
    reg [`Byte] data_mem[(1<<`DataBusWidth)-1:0];
    
    always @(posedge clk) begin
        if(ce == `Disable) begin
            //data_o <= ZeroWord;
        end else if(we == `Enable) begin
            if(sel[7] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+7] <= data_i[63:56];
            if(sel[6] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+6] <= data_i[55:48];
            if(sel[5] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+5] <= data_i[47:40];
            if(sel[4] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+4] <= data_i[39:32];
            if(sel[3] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+3] <= data_i[31:24];
            if(sel[2] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+2] <= data_i[23:16];
            if(sel[1] == 1'b1) data_mem[addr[`DataBusWidth-1:0]+1] <= data_i[15:8];
            if(sel[0] == 1'b1) data_mem[addr[`DataBusWidth-1:0]] <= data_i[7:0];
        end
    end

    always @(*) begin
        if (ce == `Disable) begin
            data_o = `ZeroDWord;
        end else if(we == `Disable) begin
            data_o = {  data_mem[addr[`DataBusWidth-1:0]+7],
                        data_mem[addr[`DataBusWidth-1:0]+6],
                        data_mem[addr[`DataBusWidth-1:0]+5],
                        data_mem[addr[`DataBusWidth-1:0]+4],
                        data_mem[addr[`DataBusWidth-1:0]+3],
                        data_mem[addr[`DataBusWidth-1:0]+2],
                        data_mem[addr[`DataBusWidth-1:0]+1],
                        data_mem[addr[`DataBusWidth-1:0]]};
        end else begin
            data_o = `ZeroDWord;
        end
    end
endmodule
