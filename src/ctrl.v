/* verilator lint_off UNOPTFLAT */

module ctrl (
    input wire rst,
    input wire stallreq_id,
    input wire stallreq_ex,
    input wire stallreq_mem,

    output reg [`STALL_SIZE] stall
);

    // stall[0] PC
    // stall[1] IF_ID
    // stall[2] ID_EX
    // stall[3] EX_MEM
    // stall[4] MEM_WB
    // stall[5] WB_REG

    always @(*) begin
        if(rst == `RESET) begin
            stall = 6'b000000;
        end else if(stallreq_mem == `Enable) begin //stop by Mem
            stall = 6'b011111;
        end else if(stallreq_ex == `Enable) begin //stop by EX
            stall = 6'b001111;
        end else if(stallreq_id == `Enable) begin //stop by ID
            stall = 6'b000111;
        end else begin
            stall = 6'b000000;
        end
    end

endmodule
