/* verilator lint_off UNUSED */
module pc_reg(
    input wire clk,
    input wire rst,
    input wire [`STALL_SIZE] stall,
    input wire branch_flag,
    input wire [`PC_SIZE] branch_target_address,

    output reg[`PC_SIZE] pc,
    output reg ce
);
    
    always @(posedge clk) begin
        if (rst == `RESET) begin
            ce <= `Disable;
        end else begin
            ce <= `Enable;
        end
    end
    
    always @(posedge clk) begin
        if (ce == `Disable) begin
            pc   <= `PC_StartPoint;
        end else if (stall[0] == `Disable) begin
            if (branch_flag == `Enable) begin
                pc <= branch_target_address;
            end else begin
                pc <= pc+64'd4;
            end
        end
    end
endmodule
    
