/* verilator lint_off UNOPTFLAT */

module register(
    input wire clk,
    input wire rst,
    input wire we,
    input wire [`REG_ADDR_SIZE] waddr,
    input wire [`REG_SIZE] wdata,
    input wire re1,
    input wire [`REG_ADDR_SIZE] raddr1,
    input wire re2,
    input wire [`REG_ADDR_SIZE] raddr2,

    output reg[`REG_SIZE] rdata1,
    output reg[`REG_SIZE] rdata2
);

    reg[`REG_SIZE] regs[0:31];
    
    always @(posedge clk) begin
        if (rst != `RESET) begin
            if (we == `Enable && waddr != 5'h0) begin
                regs[waddr] <= wdata;
            end
        end
    end
    
    always @(*) begin
        if (rst == `RESET) begin
            rdata1 = `ZeroDWord;
        end else if (raddr1 == `ZeroRegAddr) begin
            rdata1 = `ZeroDWord;
        end else if ((raddr1 == waddr) && (we == `Enable) && (re1 == `Enable)) begin
            rdata1 = wdata;
        end else if (re1 == `Enable) begin
            rdata1 = regs[raddr1];
        end else begin
            rdata1 = `ZeroDWord;
        end
    end
    
    always @(*) begin
        if (rst == `RESET) begin
            rdata2 = `ZeroDWord;
        end else if (raddr2 == 5'h0) begin
            rdata2 = `ZeroDWord;
        end else if ((raddr2 == waddr) && (we == `Enable) && (re2 == `Enable)) begin
            rdata2 = wdata;
        end else if (re2 == `Enable) begin
            rdata2 = regs[raddr2];
        end else begin
            rdata2 = `ZeroDWord;
        end
    end
endmodule
