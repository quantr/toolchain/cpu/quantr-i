/* verilator lint_off UNUSED */
module ex_mem (
    input wire clk, 
    input wire rst,
    input wire [`REG_ADDR_SIZE] ex_wd,
    input wire ex_wreg,
    input wire [`REG_SIZE] ex_wdata,
    input wire [`AluOpBus] ex_aluop,
    input wire [`REG_SIZE] ex_reg2,
    input wire [`MEM_ADDR_SIZE] mem_addr_i,
    input wire [`STALL_SIZE] stall,
    input wire [`STALL_CYCLE] stall_cycle_i,
    input wire bubble,

    output reg [`REG_ADDR_SIZE] mem_wd,
    output reg mem_wreg,
    output reg [`REG_SIZE] mem_wdata,
    output reg [`AluOpBus] mem_aluop,
    output reg [`REG_SIZE] mem_reg2,
    output reg [`STALL_CYCLE] stall_cycle,
    output reg [`MEM_ADDR_SIZE] mem_addr_o
);
    `define EXMEM_SETTINGS(aluop, r2data, we, wa, wdata, memaddr, cycle) \
        mem_aluop   <= aluop; \
        mem_reg2    <= r2data; \
        mem_wreg    <= we; \
        mem_wd      <= wa; \
        mem_wdata   <= wdata; \
        mem_addr_o  <= memaddr; \
        stall_cycle <= cycle;

    always @(posedge clk) begin
        if( rst == `RESET) begin
            `EXMEM_SETTINGS(`EXE_NOP_OP,`ZeroDWord,`Disable,`ZeroRegAddr,`ZeroDWord,`ZeroMemAddr,`ZeroCycle)
        end else if(stall[3]== `Enable && stall[4] == `Disable && bubble == `Disable) begin
            `EXMEM_SETTINGS(`EXE_NOP_OP,`ZeroDWord,`Disable,`ZeroRegAddr,`ZeroDWord,`ZeroMemAddr,stall_cycle_i)
        end else if(stall[3]== `Enable && stall[4] == `Disable && bubble == `Enable && stall_cycle_i == 2'd1) begin
            `EXMEM_SETTINGS(ex_aluop,ex_reg2,ex_wreg,ex_wd,ex_wdata,mem_addr_i,stall_cycle_i)
        end else if(stall[3] == `Disable) begin
            `EXMEM_SETTINGS(ex_aluop,ex_reg2,ex_wreg,ex_wd,ex_wdata,mem_addr_i,`ZeroCycle)
        end else begin
            stall_cycle <= stall_cycle_i;
        end
    end 
endmodule
