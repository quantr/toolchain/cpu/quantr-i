/* verilator lint_off UNUSED */
module id_ex(
    input wire clk,
    input wire rst,
    input wire[`AluOpBus] id_aluop_i,
    input wire[`AluSelBus] id_alusel_i,
    input wire[`REG_SIZE] id_reg1_i,
    input wire[`REG_SIZE] id_reg2_i,
    input wire[`REG_ADDR_SIZE] id_wd_i,
    input wire id_wreg_i,
    input wire [`STALL_SIZE] stall,
    input wire [`STALL_CYCLE] stall_cycle_i,
    input wire bubble,
    input wire [`MEM_ADDR_SIZE] mem_addr_i,
    input wire [`PC_SIZE] link_address,
    input wire need_flush_after_branch,
    
    output reg[`AluOpBus] ex_aluop_o,
    output reg[`AluSelBus] ex_alusel_o,
    output reg[`REG_SIZE] ex_reg1_o,
    output reg[`REG_SIZE] ex_reg2_o,
    output reg[`REG_ADDR_SIZE] ex_wd_o,
    output reg ex_wreg_o,
    output reg [`STALL_CYCLE] stall_cycle,
    output reg [`MEM_ADDR_SIZE] mem_addr_o,
    output reg [`PC_SIZE] link_address_o
);
    `define IDEX_SETTINGS(aluop, alusel, reg1, reg2, we, wa, memaddr, cycle, link) \
        ex_aluop_o  <= aluop; \
        ex_alusel_o <= alusel; \
        ex_reg1_o   <= reg1; \
        ex_reg2_o   <= reg2; \
        ex_wd_o     <= wa; \
        ex_wreg_o   <= we; \
        mem_addr_o  <= memaddr; \
        stall_cycle <= cycle; \
        link_address_o<= link;
    
    always @ (posedge clk) begin
        if (rst == `RESET || need_flush_after_branch == `Enable) begin
            `IDEX_SETTINGS(`EXE_NOP_OP,`EXE_RES_NOP,`ZeroDWord,`ZeroDWord,`Disable,`ZeroRegAddr,`ZeroMemAddr,`ZeroCycle,`ZeroDWord)
        end else if(stall[2]== `Enable && stall[3] == `Disable && bubble == `Disable) begin //Pipeline stopped by id module, not bubble operation, stop all data flow starting from id
            `IDEX_SETTINGS(`EXE_NOP_OP,`EXE_RES_NOP,`ZeroDWord,`ZeroDWord,`Disable,`ZeroRegAddr,`ZeroMemAddr,stall_cycle_i,`ZeroDWord)
        end else if(stall[2]== `Enable && stall[3] == `Disable && bubble == `Enable && stall_cycle_i == 2'd1) begin //Pipeline stopped by id module, bubble operation, let data pass on first round only
            `IDEX_SETTINGS(id_aluop_i,id_alusel_i,id_reg1_i,id_reg2_i,id_wreg_i,id_wd_i,mem_addr_i,stall_cycle_i,link_address)
        end else if(stall[2] == `Disable) begin
            `IDEX_SETTINGS(id_aluop_i,id_alusel_i,id_reg1_i,id_reg2_i,id_wreg_i,id_wd_i,mem_addr_i,`ZeroCycle,link_address)
        end else begin
            stall_cycle <= stall_cycle_i;
        end
    end
endmodule
    
