/* verilator lint_off UNOPTFLAT */

`include "constant.v"
module ex(input wire rst,
          input wire [`AluOpBus] aluop_i,
          input wire [`AluSelBus] aluse_i,
          input wire [`REG_SIZE] reg1_i,
          input wire [`REG_SIZE] reg2_i,
          input wire [`REG_ADDR_SIZE] wd_i,
          input wire wreg_i,
          input wire [1:0] stall_cycle_i,
          input wire [`MEM_ADDR_SIZE] mem_addr_i,
          input wire [`PC_SIZE] link_address,
          output reg [`REG_ADDR_SIZE] wd_o,
          output reg wreg_o,
          output reg [`REG_SIZE] wdata_o,
          output reg bubble,
          output reg stallreq,
          output reg [1:0] stall_cycle,
          output wire[`AluOpBus] aluop_o,
          output wire[`MEM_ADDR_SIZE] mem_addr_o,
          output wire[`REG_SIZE] reg2_o);
    
    reg [`REG_SIZE] logicout;
    reg [`REG_SIZE] shiftout;
    reg [`REG_SIZE] arithout;
    reg [`REG_SIZE] jumpout;
    
    wire [`REG_SIZE] reg2_i_mux;
    wire [`REG_SIZE] result_sum;
    
    assign reg2_i_mux        = ((aluop_i == `EXE_I_SUB) || (aluop_i == `EXE_I_SUBW) || (aluop_i == `EXE_I_SLT)) ? (~reg2_i)+1 : reg2_i;
    assign result_sum        = reg1_i + reg2_i_mux;
    assign aluop_o           = aluop_i;
    assign mem_addr_o        = mem_addr_i;
    // assign link_address_o = link_address;
    assign reg2_o            = reg2_i;
    
    //EXE_RES_LOGIC
    always @(*) begin
        if (rst == `RESET || aluse_i != `EXE_RES_LOGIC) begin
            logicout = `ZeroDWord;
            `STALL_SETTINGS(0,0,stall_cycle_i)
            end else begin
            case (aluop_i)
                //Logical Instructions
                `EXE_I_OR: begin
                    logicout = reg1_i | reg2_i;
                end
                `EXE_I_AND : begin
                    logicout = reg1_i & reg2_i;
                end
                `EXE_I_XOR : begin
                    logicout = reg1_i ^ reg2_i;
                end
                default: begin
                    logicout = `ZeroDWord;
                end
            endcase
        end
    end
    
    //`EXE_RES_SHIFT
    always @(*) begin
        if (rst == `RESET || aluse_i != `EXE_RES_SHIFT) begin
            shiftout = `ZeroDWord;
            `STALL_SETTINGS(0,0,stall_cycle_i)
            end else begin
            case (aluop_i)
                //Shift Logic Operations (Logical/Arithmetic)
                `EXE_I_SLL : begin
                    shiftout = reg1_i << reg2_i[5:0];
                end
                `EXE_I_SLLW : begin
                    shiftout[31:0] = reg1_i[31:0] << reg2_i[4:0];
                    shiftout       = {{32{shiftout[31]}},shiftout[31:0]};
                end
                `EXE_I_SRL : begin
                    shiftout = reg1_i >> reg2_i[5:0];
                end
                `EXE_I_SRLW : begin
                    shiftout[31:0] = reg1_i[31:0] >> reg2_i[4:0];
                    shiftout       = {{32{shiftout[31]}},shiftout[31:0]};
                end
                `EXE_I_SRA : begin
                    shiftout = ({64{reg1_i[63]}} << (7'd64 - {1'b0,reg2_i[5:0]})) | reg1_i[63:0] >> reg2_i[5:0];
                end
                `EXE_I_SRAW : begin
                    shiftout = ({64{reg1_i[31]}} << (6'd32 - {1'b0,reg2_i[4:0]})) | {32'h00000000,reg1_i[31:0] >> reg2_i[4:0]};
                end
                default: begin
                    shiftout = `ZeroDWord;
                end
            endcase
        end
    end
    
    //EXE_RES_ARITH
    always @(*) begin
        if (rst == `RESET || aluse_i != `EXE_RES_ARITH) begin
            arithout = `ZeroDWord;
            `STALL_SETTINGS(0,0,stall_cycle_i)
            end else begin
            case (aluop_i)
                //Basic Arithmetic Instructions
                `EXE_I_ADD, `EXE_I_SUB : begin
                    arithout = result_sum;
                end
                `EXE_I_ADDW, `EXE_I_SUBW : begin
                    arithout = {{32{result_sum[31]}},result_sum[31:0]};
                end
                //Set Less Than instructions
                `EXE_I_SLT : begin
                    arithout = {63'd0,((reg1_i[63] && !reg2_i[63]) || (!reg1_i[63] && !reg2_i[63] && result_sum[63]) || (reg1_i[63] && reg2_i[63] && result_sum[63]))};
                end
                `EXE_I_SLTU : begin
                    arithout = {63'd0,(reg1_i < reg2_i)};
                end
                default: begin
                    arithout = `ZeroDWord;
                end
            endcase
        end
    end
    
    //EXE_RES_JUMP
    always @(*) begin
        if (rst == `RESET || aluse_i != `EXE_RES_JUMP) begin
            jumpout = `ZeroDWord;
            `STALL_SETTINGS(0,0,stall_cycle_i)
            end else begin
            case (aluop_i)
                `EXE_I_JAL, `EXE_I_JALR : begin
                    jumpout = link_address;
                end
                default: begin
                    jumpout = `ZeroDWord;
                end
            endcase
        end
    end
    
    //Send data to mem module
    always @(*) begin
        wd_o   = wd_i;
        wreg_o = wreg_i;
        case (aluse_i)
            `EXE_RES_LOGIC: begin
                wdata_o = logicout;
            end
            `EXE_RES_SHIFT: begin
                wdata_o = shiftout;
            end
            `EXE_RES_ARITH: begin
                wdata_o = arithout;
            end
            `EXE_RES_JUMP: begin
                wdata_o = jumpout;
            end
            default: begin
                wdata_o = `ZeroDWord;
            end
        endcase
    end
    
endmodule
