#include <verilated_vcd_c.h>

#include "Vquantr_i_main.h"
#include "verilated.h"
#include "Vquantr_i_main__Dpi.h"

int clk;
int myTime = 0;
FILE *fp;

int add(int a, int b)
{
    printf("add ar\n");
    return a + b;
}

void profile(const char *file, int lineNo, const char *str, long long value)
{
    if (clk == 1)
    {
        printf("file=%s,\t lineNo=%d,\t time=%d,\t%s=%llx\n", file, lineNo, myTime, str, value);
        fprintf(fp, "file=%s,\t lineNo=%d,\t time=%d,\t%s=%llx\n", file, lineNo, myTime, str, value);
    }
}

vluint64_t main_time = 0;
VerilatedVcdC *tfp;

double sc_time_stamp() { return main_time; }

// FILE *fd;
// extern "C" int saveData(char *s) {
//     if (fd == 0) {
//         fd = fopen("data.txt", "a");
//     }
//     char buf[1024];
//     sscanf(s, "%s\n", buf);
//     fwrite(buf, 1, strlen(buf), fd);
//     return (0);
// }

int main(int argc, char **argv, char **env)
{
    Verilated::commandArgs(argc, argv);
    Vquantr_i_main *counter = new Vquantr_i_main;

    fp = fopen("quantr.profiling", "w");

    Verilated::traceEverOn(true);
    tfp = new VerilatedVcdC;
    counter->trace(tfp, 99);
    tfp->open("output.vcd");

    // while (!Verilated::gotFinish())
    for (main_time = 0; main_time < 100; main_time++)
    {
        clk = main_time % 2;
        myTime++;
        counter->clk = clk;
        counter->eval();
        tfp->dump(main_time);
    }
    counter->final();
    //Verilated::threadContextp()->coveragep()->write("coverage.txt");
    delete counter;

    tfp->close();

    fclose(fp);
    exit(0);
}
