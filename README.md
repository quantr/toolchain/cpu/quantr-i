# Quantr I

RISC-V cpu design, first attempt
git link: https://gitlab.com/quantr/toolchain/cpu/quantr-i.git

## Goal

We want to know how the CPU works, the best way is to building on. We use RISC-V as the ISA and try to develop our own soft core running on FPGA. In the same time we will look for tape out chance, efabless is our current direction

## Join the team

Make sure you can code and are hands-on, we don't accept people just talking (there are too many of these in Hong Kong). 

Below is the skill set you will need

1. Verilog
2. Verilator (C/C++)
3. Skill to use FPGA board. We use Xilinx and Lattice
4. Java (Our own toolchain in RISC-V are all in Java)

Tape out skills:

1. E-fabless
2. Openlane
3. Synopsys

We regularly recruit students to helpout some tasks, ask Peter for anthing if you are interested.

## License

This project included every tool we built are in open source

## Author

Peter <peter@quantr.hk>

Darren

Carrie

Derek

Walter

Lucas

