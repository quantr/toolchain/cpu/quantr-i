#include "Vex.h"
#include "verilated.h"
#include <verilated_vcd_c.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <boost/lambda/lambda.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/unordered_map.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
using namespace boost::posix_time;
using namespace std;

vluint64_t main_time = 0;
VerilatedVcdC *tfp;

// double sc_time_stamp() { return main_time; }

template <typename ElemT>
struct HexTo
{
	ElemT value;
	operator ElemT() const { return value; }
	friend std::istream &operator>>(std::istream &in, HexTo &out)
	{
		in >> std::hex >> out.value;
		return in;
	}
};

unsigned long long strToLongLong(string str)
{
	if (boost::contains(str, "0x"))
	{
		return boost::lexical_cast<HexTo<unsigned long long>>(str);
	}
	else
	{
		return boost::lexical_cast<unsigned long long>(str);
	}
}

unsigned long long gen(int x)
{
	unsigned long long temp = 1;
	for (int z = 0; z < x; z++)
	{
		temp = temp << 1 | 1;
	}
	return temp;
}

unsigned long long getValue(string str, boost::unordered_map<string, string> umap, boost::unordered_map<string, boost::multiprecision::uint128_t> dynamicVariable_values)
{
	// cout << "\t\t\tstr=" << str;
	if (boost::contains(umap[str], "$"))
	{
		// cout << "---" << dynamicVariable_values[str] << endl;
		return boost::lexical_cast<unsigned long long>(dynamicVariable_values[str]);
	}
	else
	{
		// cout << "+++" << umap[str] << endl;
		return boost::lexical_cast<unsigned long long>(umap[str]);
	}
}

int checkResult(Vex *ex, string str)
{
	if (str == "sll")
	{
    
		printf("\t\t\t\tex->reg1_i =%lx\n", ex->reg1_i);
		printf("\t\t\t\tex->reg2_i & 0x3f =%lx\n", ex->reg2_i & 0x3f);
		printf("\t\t\t\tex->wdata_o =%lx\n", ex->wdata_o);
        

		if (ex->wdata_o == (ex->reg1_i << (ex->reg2_i & 0x3f)))
		{
			return 1;
		}
	}
	return 0;
}

int main(int argc, char **argv, char **env)
{
	ptime t1 = boost::posix_time::second_clock::local_time();
	ifstream file("sll.txt", ios_base::in | ios_base::binary);
	boost::iostreams::filtering_istream in;
	in.push(file);
	boost::unordered_map<string, string> umap;

	vector<string> dynamicVariable_names;
	boost::unordered_map<string, boost::multiprecision::uint128_t> dynamicVariable_values;
	// vector<boost::multiprecision::uint128_t> dynamicVariable_values;
	vector<unsigned long long> dynamicVariable_maxValues;
	vector<unsigned long long> dynamicVariable_increments;

	for (string line; getline(in, line);)
	{
		// cout << line << endl;
		vector<string> strs;
		boost::split(strs, line, boost::is_any_of("="));

		if (strs.size() == 2)
		{
			string wireName = strs[0];
			umap[wireName] = strs[1];
			if (boost::starts_with(wireName, "input."))
			{
				cout << wireName << "\t=\t" << strs[1] << endl;
				if (boost::contains(strs[1], "$"))
				{
					dynamicVariable_names.push_back(wireName);
					dynamicVariable_values[wireName] = 0;

					// increment
					string temp = strs[1];
					boost::replace_all(temp, "${", "");
					boost::replace_all(temp, "}", "");
					vector<string> tempVector;
					boost::split(tempVector, temp, boost::is_any_of(","));
					// cout << "tempVector[0]=" << tempVector[0] << endl;
					unsigned long long width = strToLongLong(tempVector[0]);
					// cout << "maxValue=" << maxValue << endl;
					unsigned long long increment = strToLongLong(tempVector[1]);

					unsigned long long temp2 = gen(width);
					dynamicVariable_maxValues.push_back(temp2);
					dynamicVariable_increments.push_back(increment);
					// cout << "temp=" << increment << endl;
				}
			}
		}
	}

	Verilated::commandArgs(argc, argv);
	Vex *ex = new Vex;

	Verilated::traceEverOn(true);
	tfp = new VerilatedVcdC;
	ex->trace(tfp, 99);
	tfp->open("output.vcd");

	int index = 0;
	while (true)
	{
		ex->rst = getValue("input.rst", umap, dynamicVariable_values);
		ex->aluop_i = getValue("input.aluop_i", umap, dynamicVariable_values);
		ex->aluse_i = getValue("input.aluse_i", umap, dynamicVariable_values);
		ex->reg1_i = getValue("input.reg1_i", umap, dynamicVariable_values);
		ex->reg2_i = getValue("input.reg2_i", umap, dynamicVariable_values);
		ex->wd_i = getValue("input.wd_i", umap, dynamicVariable_values);
		ex->wreg_i = getValue("input.wreg_i", umap, dynamicVariable_values);
		ex->imm = getValue("input.imm", umap, dynamicVariable_values);
		ex->pc_in = getValue("input.pc_in", umap, dynamicVariable_values);
		ex->eval();
		tfp->dump(main_time++);

		int r = checkResult(ex, umap["result"]);
		if (r == 0)
		{
			cout << "error" << endl;
			exit(0);
		}

		for (int x = 0; x < dynamicVariable_names.size(); x++)
		{
			string wireName = dynamicVariable_names[x];
			cout << index << " : " << std::string(x, '\t') << wireName << " = " << dynamicVariable_values[wireName] << endl;
			dynamicVariable_values[wireName] += dynamicVariable_increments[x];

			if (dynamicVariable_values[wireName] > dynamicVariable_maxValues[x] && x == dynamicVariable_names.size() - 1)
			{
				goto end1;
			}
			else if (dynamicVariable_values[wireName] > dynamicVariable_maxValues[x])
			{
				dynamicVariable_values[wireName] = 0;
			}
			else
			{
				break;
			}
		}
		index++;
	}

end1:

	ex->final();
	delete ex;

	tfp->close();

	ptime t2 = boost::posix_time::second_clock::local_time();
	time_duration t3 = t2 - t1;
	cout << t3.total_seconds() << " sec" << endl;
	cout << "success" << endl;
	exit(0);
}
