// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vex.h for the primary calling header

#ifndef VERILATED_VEX___024ROOT_H_
#define VERILATED_VEX___024ROOT_H_  // guard

#include "verilated.h"

class Vex__Syms;
VL_MODULE(Vex___024root) {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(rst,0,0);
    VL_IN8(aluse_i,2,0);
    VL_IN8(wd_i,4,0);
    VL_IN8(wreg_i,0,0);
    VL_IN8(stall_cycle_i,1,0);
    VL_OUT8(wd_o,4,0);
    VL_OUT8(wreg_o,0,0);
    VL_OUT8(bubble,0,0);
    VL_OUT8(stallreq,0,0);
    VL_OUT8(stall_cycle,1,0);
    CData/*1:0*/ __Vtask_ex__DOT__SET_STALL__0__clks_to_stop;
    CData/*0:0*/ __Vtask_ex__DOT__SET_STALL__0__insert_bubble;
    VL_IN16(aluop_i,15,0);
    VL_IN64(reg1_i,63,0);
    VL_IN64(reg2_i,63,0);
    VL_OUT64(wdata_o,63,0);
    QData/*63:0*/ ex__DOT__shiftout;
    QData/*63:0*/ ex__DOT__result_sum;

    // INTERNAL VARIABLES
    Vex__Syms* vlSymsp;  // Symbol table

    // CONSTRUCTORS
    Vex___024root(const char* name);
    ~Vex___024root();
    VL_UNCOPYABLE(Vex___024root);

    // INTERNAL METHODS
    void __Vconfigure(Vex__Syms* symsp, bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
