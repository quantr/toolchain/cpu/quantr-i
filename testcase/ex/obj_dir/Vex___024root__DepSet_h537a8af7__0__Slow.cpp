// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vex.h for the primary calling header

#include "verilated.h"

#include "Vex___024root.h"

VL_ATTR_COLD void Vex___024root___eval_initial(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___eval_initial\n"); );
}

void Vex___024root___combo__TOP__1(Vex___024root* vlSelf);

VL_ATTR_COLD void Vex___024root___eval_settle(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___eval_settle\n"); );
    // Body
    Vex___024root___combo__TOP__1(vlSelf);
}

VL_ATTR_COLD void Vex___024root___final(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___final\n"); );
}

VL_ATTR_COLD void Vex___024root___ctor_var_reset(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___ctor_var_reset\n"); );
    // Body
    vlSelf->rst = VL_RAND_RESET_I(1);
    vlSelf->aluop_i = VL_RAND_RESET_I(16);
    vlSelf->aluse_i = VL_RAND_RESET_I(3);
    vlSelf->reg1_i = VL_RAND_RESET_Q(64);
    vlSelf->reg2_i = VL_RAND_RESET_Q(64);
    vlSelf->wd_i = VL_RAND_RESET_I(5);
    vlSelf->wreg_i = VL_RAND_RESET_I(1);
    vlSelf->stall_cycle_i = VL_RAND_RESET_I(2);
    vlSelf->wd_o = VL_RAND_RESET_I(5);
    vlSelf->wreg_o = VL_RAND_RESET_I(1);
    vlSelf->wdata_o = VL_RAND_RESET_Q(64);
    vlSelf->bubble = VL_RAND_RESET_I(1);
    vlSelf->stallreq = VL_RAND_RESET_I(1);
    vlSelf->stall_cycle = VL_RAND_RESET_I(2);
    vlSelf->ex__DOT__shiftout = VL_RAND_RESET_Q(64);
    vlSelf->ex__DOT__result_sum = VL_RAND_RESET_Q(64);
    vlSelf->__Vtask_ex__DOT__SET_STALL__0__clks_to_stop = VL_RAND_RESET_I(2);
    vlSelf->__Vtask_ex__DOT__SET_STALL__0__insert_bubble = VL_RAND_RESET_I(1);
}
