// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vex.h for the primary calling header

#include "verilated.h"

#include "Vex___024root.h"

VL_INLINE_OPT void Vex___024root___combo__TOP__1(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___combo__TOP__1\n"); );
    // Body
    vlSelf->wreg_o = vlSelf->wreg_i;
    vlSelf->wd_o = vlSelf->wd_i;
    if (((IData)(vlSelf->rst) | (2U != (IData)(vlSelf->aluse_i)))) {
        vlSelf->stall_cycle = vlSelf->stall_cycle_i;
        vlSelf->stallreq = 0U;
        vlSelf->bubble = 0U;
    } else {
        vlSelf->stall_cycle = vlSelf->stall_cycle_i;
        vlSelf->stallreq = 0U;
        vlSelf->bubble = 0U;
        if ((0xaU == (IData)(vlSelf->aluop_i))) {
            vlSelf->__Vtask_ex__DOT__SET_STALL__0__insert_bubble = 1U;
            vlSelf->__Vtask_ex__DOT__SET_STALL__0__clks_to_stop = 2U;
            if ((2U > (IData)(vlSelf->stall_cycle))) {
                vlSelf->stallreq = 1U;
                vlSelf->stall_cycle = (3U & ((IData)(1U) 
                                             + (IData)(vlSelf->stall_cycle_i)));
                if (vlSelf->__Vtask_ex__DOT__SET_STALL__0__insert_bubble) {
                    vlSelf->bubble = 1U;
                }
            } else if (((IData)(vlSelf->stall_cycle) 
                        == (IData)(vlSelf->__Vtask_ex__DOT__SET_STALL__0__clks_to_stop))) {
                vlSelf->stall_cycle = 0U;
                vlSelf->stallreq = 0U;
                vlSelf->bubble = 0U;
            }
        }
    }
    vlSelf->ex__DOT__result_sum = (vlSelf->reg1_i + 
                                   ((((5U == (IData)(vlSelf->aluop_i)) 
                                      | (9U == (IData)(vlSelf->aluop_i))) 
                                     | (6U == (IData)(vlSelf->aluop_i)))
                                     ? (1ULL + (~ vlSelf->reg2_i))
                                     : vlSelf->reg2_i));
    if (((IData)(vlSelf->rst) | (2U != (IData)(vlSelf->aluse_i)))) {
        vlSelf->ex__DOT__shiftout = 0ULL;
    } else if ((0xaU == (IData)(vlSelf->aluop_i))) {
        vlSelf->ex__DOT__shiftout = (vlSelf->reg1_i 
                                     << (0x3fU & (IData)(vlSelf->reg2_i)));
    } else if ((0xdU == (IData)(vlSelf->aluop_i))) {
        vlSelf->ex__DOT__shiftout = ((0xffffffff00000000ULL 
                                      & vlSelf->ex__DOT__shiftout) 
                                     | (IData)((IData)(
                                                       ((IData)(vlSelf->reg1_i) 
                                                        << 
                                                        (0x1fU 
                                                         & (IData)(vlSelf->reg2_i))))));
        vlSelf->ex__DOT__shiftout = (((QData)((IData)(
                                                      (- (IData)(
                                                                 (1U 
                                                                  & (IData)(
                                                                            (vlSelf->ex__DOT__shiftout 
                                                                             >> 0x1fU))))))) 
                                      << 0x20U) | (QData)((IData)(vlSelf->ex__DOT__shiftout)));
    } else if ((0xbU == (IData)(vlSelf->aluop_i))) {
        vlSelf->ex__DOT__shiftout = (vlSelf->reg1_i 
                                     >> (0x3fU & (IData)(vlSelf->reg2_i)));
    } else if ((0xeU == (IData)(vlSelf->aluop_i))) {
        vlSelf->ex__DOT__shiftout = ((0xffffffff00000000ULL 
                                      & vlSelf->ex__DOT__shiftout) 
                                     | (IData)((IData)(
                                                       ((IData)(vlSelf->reg1_i) 
                                                        >> 
                                                        (0x1fU 
                                                         & (IData)(vlSelf->reg2_i))))));
        vlSelf->ex__DOT__shiftout = (((QData)((IData)(
                                                      (- (IData)(
                                                                 (1U 
                                                                  & (IData)(
                                                                            (vlSelf->ex__DOT__shiftout 
                                                                             >> 0x1fU))))))) 
                                      << 0x20U) | (QData)((IData)(vlSelf->ex__DOT__shiftout)));
    } else {
        vlSelf->ex__DOT__shiftout = ((0xcU == (IData)(vlSelf->aluop_i))
                                      ? (((0x3fU >= 
                                           (0x7fU & 
                                            ((IData)(0x40U) 
                                             - (0x3fU 
                                                & (IData)(vlSelf->reg2_i)))))
                                           ? ((- (QData)((IData)(
                                                                 (1U 
                                                                  & (IData)(
                                                                            (vlSelf->reg1_i 
                                                                             >> 0x3fU)))))) 
                                              << (0x7fU 
                                                  & ((IData)(0x40U) 
                                                     - 
                                                     (0x3fU 
                                                      & (IData)(vlSelf->reg2_i)))))
                                           : 0ULL) 
                                         | (vlSelf->reg1_i 
                                            >> (0x3fU 
                                                & (IData)(vlSelf->reg2_i))))
                                      : ((0xfU == (IData)(vlSelf->aluop_i))
                                          ? (((- (QData)((IData)(
                                                                 (1U 
                                                                  & (IData)(
                                                                            (vlSelf->reg1_i 
                                                                             >> 0x1fU)))))) 
                                              << (0x3fU 
                                                  & ((IData)(0x20U) 
                                                     - 
                                                     (0x1fU 
                                                      & (IData)(vlSelf->reg2_i))))) 
                                             | (QData)((IData)(
                                                               ((IData)(vlSelf->reg1_i) 
                                                                >> 
                                                                (0x1fU 
                                                                 & (IData)(vlSelf->reg2_i))))))
                                          : 0ULL));
    }
    vlSelf->wdata_o = ((1U == (IData)(vlSelf->aluse_i))
                        ? (((IData)(vlSelf->rst) | 
                            (1U != (IData)(vlSelf->aluse_i)))
                            ? 0ULL : ((2U == (IData)(vlSelf->aluop_i))
                                       ? (vlSelf->reg1_i 
                                          | vlSelf->reg2_i)
                                       : ((1U == (IData)(vlSelf->aluop_i))
                                           ? (vlSelf->reg1_i 
                                              & vlSelf->reg2_i)
                                           : ((3U == (IData)(vlSelf->aluop_i))
                                               ? (vlSelf->reg1_i 
                                                  ^ vlSelf->reg2_i)
                                               : 0ULL))))
                        : ((2U == (IData)(vlSelf->aluse_i))
                            ? vlSelf->ex__DOT__shiftout
                            : ((3U == (IData)(vlSelf->aluse_i))
                                ? (((IData)(vlSelf->rst) 
                                    | (3U != (IData)(vlSelf->aluse_i)))
                                    ? 0ULL : (((4U 
                                                == (IData)(vlSelf->aluop_i)) 
                                               | (5U 
                                                  == (IData)(vlSelf->aluop_i)))
                                               ? vlSelf->ex__DOT__result_sum
                                               : ((
                                                   (8U 
                                                    == (IData)(vlSelf->aluop_i)) 
                                                   | (9U 
                                                      == (IData)(vlSelf->aluop_i)))
                                                   ? 
                                                  (((QData)((IData)(
                                                                    (- (IData)(
                                                                               (1U 
                                                                                & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x1fU))))))) 
                                                    << 0x20U) 
                                                   | (QData)((IData)(vlSelf->ex__DOT__result_sum)))
                                                   : 
                                                  ((6U 
                                                    == (IData)(vlSelf->aluop_i))
                                                    ? (QData)((IData)(
                                                                      (1U 
                                                                       & ((((IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU)) 
                                                                            & (~ (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU)))) 
                                                                           | (((~ (IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU))) 
                                                                               & (~ (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU)))) 
                                                                              & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x3fU)))) 
                                                                          | (((IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU)) 
                                                                              & (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU))) 
                                                                             & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x3fU)))))))
                                                    : 
                                                   ((7U 
                                                     == (IData)(vlSelf->aluop_i))
                                                     ? (QData)((IData)(
                                                                       (vlSelf->reg1_i 
                                                                        < vlSelf->reg2_i)))
                                                     : 0ULL)))))
                                : 0ULL)));
}

void Vex___024root___eval(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___eval\n"); );
    // Body
    Vex___024root___combo__TOP__1(vlSelf);
}

#ifdef VL_DEBUG
void Vex___024root___eval_debug_assertions(Vex___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root___eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((vlSelf->rst & 0xfeU))) {
        Verilated::overWidthError("rst");}
    if (VL_UNLIKELY((vlSelf->aluse_i & 0xf8U))) {
        Verilated::overWidthError("aluse_i");}
    if (VL_UNLIKELY((vlSelf->wd_i & 0xe0U))) {
        Verilated::overWidthError("wd_i");}
    if (VL_UNLIKELY((vlSelf->wreg_i & 0xfeU))) {
        Verilated::overWidthError("wreg_i");}
    if (VL_UNLIKELY((vlSelf->stall_cycle_i & 0xfcU))) {
        Verilated::overWidthError("stall_cycle_i");}
}
#endif  // VL_DEBUG
