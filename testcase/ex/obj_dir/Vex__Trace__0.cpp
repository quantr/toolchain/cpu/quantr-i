// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vex__Syms.h"


void Vex___024root__trace_chg_sub_0(Vex___024root* vlSelf, VerilatedVcd* tracep);

void Vex___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd* tracep) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_chg_top_0\n"); );
    // Init
    Vex___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vex___024root*>(voidSelf);
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    Vex___024root__trace_chg_sub_0((&vlSymsp->TOP), tracep);
}

void Vex___024root__trace_chg_sub_0(Vex___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_chg_sub_0\n"); );
    // Init
    vluint32_t* const oldp VL_ATTR_UNUSED = tracep->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    tracep->chgBit(oldp+0,(vlSelf->rst));
    tracep->chgSData(oldp+1,(vlSelf->aluop_i),16);
    tracep->chgCData(oldp+2,(vlSelf->aluse_i),3);
    tracep->chgQData(oldp+3,(vlSelf->reg1_i),64);
    tracep->chgQData(oldp+5,(vlSelf->reg2_i),64);
    tracep->chgCData(oldp+7,(vlSelf->wd_i),5);
    tracep->chgBit(oldp+8,(vlSelf->wreg_i));
    tracep->chgCData(oldp+9,(vlSelf->stall_cycle_i),2);
    tracep->chgCData(oldp+10,(vlSelf->wd_o),5);
    tracep->chgBit(oldp+11,(vlSelf->wreg_o));
    tracep->chgQData(oldp+12,(vlSelf->wdata_o),64);
    tracep->chgBit(oldp+14,(vlSelf->bubble));
    tracep->chgBit(oldp+15,(vlSelf->stallreq));
    tracep->chgCData(oldp+16,(vlSelf->stall_cycle),2);
    tracep->chgQData(oldp+17,((((IData)(vlSelf->rst) 
                                | (1U != (IData)(vlSelf->aluse_i)))
                                ? 0ULL : ((2U == (IData)(vlSelf->aluop_i))
                                           ? (vlSelf->reg1_i 
                                              | vlSelf->reg2_i)
                                           : ((1U == (IData)(vlSelf->aluop_i))
                                               ? (vlSelf->reg1_i 
                                                  & vlSelf->reg2_i)
                                               : ((3U 
                                                   == (IData)(vlSelf->aluop_i))
                                                   ? 
                                                  (vlSelf->reg1_i 
                                                   ^ vlSelf->reg2_i)
                                                   : 0ULL))))),64);
    tracep->chgQData(oldp+19,(vlSelf->ex__DOT__shiftout),64);
    tracep->chgQData(oldp+21,((((IData)(vlSelf->rst) 
                                | (3U != (IData)(vlSelf->aluse_i)))
                                ? 0ULL : (((4U == (IData)(vlSelf->aluop_i)) 
                                           | (5U == (IData)(vlSelf->aluop_i)))
                                           ? vlSelf->ex__DOT__result_sum
                                           : (((8U 
                                                == (IData)(vlSelf->aluop_i)) 
                                               | (9U 
                                                  == (IData)(vlSelf->aluop_i)))
                                               ? (((QData)((IData)(
                                                                   (- (IData)(
                                                                              (1U 
                                                                               & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x1fU))))))) 
                                                   << 0x20U) 
                                                  | (QData)((IData)(vlSelf->ex__DOT__result_sum)))
                                               : ((6U 
                                                   == (IData)(vlSelf->aluop_i))
                                                   ? (QData)((IData)(
                                                                     (1U 
                                                                      & ((((IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU)) 
                                                                           & (~ (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU)))) 
                                                                          | (((~ (IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU))) 
                                                                              & (~ (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU)))) 
                                                                             & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x3fU)))) 
                                                                         | (((IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU)) 
                                                                             & (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU))) 
                                                                            & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x3fU)))))))
                                                   : 
                                                  ((7U 
                                                    == (IData)(vlSelf->aluop_i))
                                                    ? (QData)((IData)(
                                                                      (vlSelf->reg1_i 
                                                                       < vlSelf->reg2_i)))
                                                    : 0ULL)))))),64);
    tracep->chgQData(oldp+23,(((((5U == (IData)(vlSelf->aluop_i)) 
                                 | (9U == (IData)(vlSelf->aluop_i))) 
                                | (6U == (IData)(vlSelf->aluop_i)))
                                ? (1ULL + (~ vlSelf->reg2_i))
                                : vlSelf->reg2_i)),64);
    tracep->chgQData(oldp+25,(vlSelf->ex__DOT__result_sum),64);
}

void Vex___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_cleanup\n"); );
    // Init
    Vex___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vex___024root*>(voidSelf);
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VlUnpacked<CData/*0:0*/, 1> __Vm_traceActivity;
    // Body
    vlSymsp->__Vm_activity = false;
    __Vm_traceActivity[0U] = 0U;
}
