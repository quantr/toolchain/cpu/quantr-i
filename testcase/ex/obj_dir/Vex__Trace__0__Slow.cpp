// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vex__Syms.h"


VL_ATTR_COLD void Vex___024root__trace_init_sub_0(Vex___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD void Vex___024root__trace_init_top(Vex___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_init_top\n"); );
    // Body
    Vex___024root__trace_init_sub_0(vlSelf, tracep);
}

VL_ATTR_COLD void Vex___024root__trace_init_sub_0(Vex___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_init_sub_0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    if (false && tracep && c) {}  // Prevent unused
    // Body
    tracep->declBit(c+1,"rst", false,-1);
    tracep->declBus(c+2,"aluop_i", false,-1, 15,0);
    tracep->declBus(c+3,"aluse_i", false,-1, 2,0);
    tracep->declQuad(c+4,"reg1_i", false,-1, 63,0);
    tracep->declQuad(c+6,"reg2_i", false,-1, 63,0);
    tracep->declBus(c+8,"wd_i", false,-1, 4,0);
    tracep->declBit(c+9,"wreg_i", false,-1);
    tracep->declBus(c+10,"stall_cycle_i", false,-1, 1,0);
    tracep->declBus(c+11,"wd_o", false,-1, 4,0);
    tracep->declBit(c+12,"wreg_o", false,-1);
    tracep->declQuad(c+13,"wdata_o", false,-1, 63,0);
    tracep->declBit(c+15,"bubble", false,-1);
    tracep->declBit(c+16,"stallreq", false,-1);
    tracep->declBus(c+17,"stall_cycle", false,-1, 1,0);
    tracep->declBit(c+1,"ex rst", false,-1);
    tracep->declBus(c+2,"ex aluop_i", false,-1, 15,0);
    tracep->declBus(c+3,"ex aluse_i", false,-1, 2,0);
    tracep->declQuad(c+4,"ex reg1_i", false,-1, 63,0);
    tracep->declQuad(c+6,"ex reg2_i", false,-1, 63,0);
    tracep->declBus(c+8,"ex wd_i", false,-1, 4,0);
    tracep->declBit(c+9,"ex wreg_i", false,-1);
    tracep->declBus(c+10,"ex stall_cycle_i", false,-1, 1,0);
    tracep->declBus(c+11,"ex wd_o", false,-1, 4,0);
    tracep->declBit(c+12,"ex wreg_o", false,-1);
    tracep->declQuad(c+13,"ex wdata_o", false,-1, 63,0);
    tracep->declBit(c+15,"ex bubble", false,-1);
    tracep->declBit(c+16,"ex stallreq", false,-1);
    tracep->declBus(c+17,"ex stall_cycle", false,-1, 1,0);
    tracep->declQuad(c+18,"ex logicout", false,-1, 63,0);
    tracep->declQuad(c+20,"ex shiftout", false,-1, 63,0);
    tracep->declQuad(c+22,"ex arithout", false,-1, 63,0);
    tracep->declQuad(c+24,"ex reg2_i_mux", false,-1, 63,0);
    tracep->declQuad(c+26,"ex result_sum", false,-1, 63,0);
}

VL_ATTR_COLD void Vex___024root__trace_full_top_0(void* voidSelf, VerilatedVcd* tracep);
void Vex___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd* tracep);
void Vex___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/);

VL_ATTR_COLD void Vex___024root__trace_register(Vex___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&Vex___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&Vex___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&Vex___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void Vex___024root__trace_full_sub_0(Vex___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD void Vex___024root__trace_full_top_0(void* voidSelf, VerilatedVcd* tracep) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_full_top_0\n"); );
    // Init
    Vex___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vex___024root*>(voidSelf);
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    Vex___024root__trace_full_sub_0((&vlSymsp->TOP), tracep);
}

VL_ATTR_COLD void Vex___024root__trace_full_sub_0(Vex___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vex__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vex___024root__trace_full_sub_0\n"); );
    // Init
    vluint32_t* const oldp VL_ATTR_UNUSED = tracep->oldp(vlSymsp->__Vm_baseCode);
    // Body
    tracep->fullBit(oldp+1,(vlSelf->rst));
    tracep->fullSData(oldp+2,(vlSelf->aluop_i),16);
    tracep->fullCData(oldp+3,(vlSelf->aluse_i),3);
    tracep->fullQData(oldp+4,(vlSelf->reg1_i),64);
    tracep->fullQData(oldp+6,(vlSelf->reg2_i),64);
    tracep->fullCData(oldp+8,(vlSelf->wd_i),5);
    tracep->fullBit(oldp+9,(vlSelf->wreg_i));
    tracep->fullCData(oldp+10,(vlSelf->stall_cycle_i),2);
    tracep->fullCData(oldp+11,(vlSelf->wd_o),5);
    tracep->fullBit(oldp+12,(vlSelf->wreg_o));
    tracep->fullQData(oldp+13,(vlSelf->wdata_o),64);
    tracep->fullBit(oldp+15,(vlSelf->bubble));
    tracep->fullBit(oldp+16,(vlSelf->stallreq));
    tracep->fullCData(oldp+17,(vlSelf->stall_cycle),2);
    tracep->fullQData(oldp+18,((((IData)(vlSelf->rst) 
                                 | (1U != (IData)(vlSelf->aluse_i)))
                                 ? 0ULL : ((2U == (IData)(vlSelf->aluop_i))
                                            ? (vlSelf->reg1_i 
                                               | vlSelf->reg2_i)
                                            : ((1U 
                                                == (IData)(vlSelf->aluop_i))
                                                ? (vlSelf->reg1_i 
                                                   & vlSelf->reg2_i)
                                                : (
                                                   (3U 
                                                    == (IData)(vlSelf->aluop_i))
                                                    ? 
                                                   (vlSelf->reg1_i 
                                                    ^ vlSelf->reg2_i)
                                                    : 0ULL))))),64);
    tracep->fullQData(oldp+20,(vlSelf->ex__DOT__shiftout),64);
    tracep->fullQData(oldp+22,((((IData)(vlSelf->rst) 
                                 | (3U != (IData)(vlSelf->aluse_i)))
                                 ? 0ULL : (((4U == (IData)(vlSelf->aluop_i)) 
                                            | (5U == (IData)(vlSelf->aluop_i)))
                                            ? vlSelf->ex__DOT__result_sum
                                            : (((8U 
                                                 == (IData)(vlSelf->aluop_i)) 
                                                | (9U 
                                                   == (IData)(vlSelf->aluop_i)))
                                                ? (
                                                   ((QData)((IData)(
                                                                    (- (IData)(
                                                                               (1U 
                                                                                & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x1fU))))))) 
                                                    << 0x20U) 
                                                   | (QData)((IData)(vlSelf->ex__DOT__result_sum)))
                                                : (
                                                   (6U 
                                                    == (IData)(vlSelf->aluop_i))
                                                    ? (QData)((IData)(
                                                                      (1U 
                                                                       & ((((IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU)) 
                                                                            & (~ (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU)))) 
                                                                           | (((~ (IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU))) 
                                                                               & (~ (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU)))) 
                                                                              & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x3fU)))) 
                                                                          | (((IData)(
                                                                                (vlSelf->reg1_i 
                                                                                >> 0x3fU)) 
                                                                              & (IData)(
                                                                                (vlSelf->reg2_i 
                                                                                >> 0x3fU))) 
                                                                             & (IData)(
                                                                                (vlSelf->ex__DOT__result_sum 
                                                                                >> 0x3fU)))))))
                                                    : 
                                                   ((7U 
                                                     == (IData)(vlSelf->aluop_i))
                                                     ? (QData)((IData)(
                                                                       (vlSelf->reg1_i 
                                                                        < vlSelf->reg2_i)))
                                                     : 0ULL)))))),64);
    tracep->fullQData(oldp+24,(((((5U == (IData)(vlSelf->aluop_i)) 
                                  | (9U == (IData)(vlSelf->aluop_i))) 
                                 | (6U == (IData)(vlSelf->aluop_i)))
                                 ? (1ULL + (~ vlSelf->reg2_i))
                                 : vlSelf->reg2_i)),64);
    tracep->fullQData(oldp+26,(vlSelf->ex__DOT__result_sum),64);
}
