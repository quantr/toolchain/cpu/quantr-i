// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vex.h for the primary calling header

#include "verilated.h"

#include "Vex__Syms.h"
#include "Vex___024root.h"

void Vex___024root___ctor_var_reset(Vex___024root* vlSelf);

Vex___024root::Vex___024root(const char* _vcname__)
    : VerilatedModule(_vcname__)
 {
    // Reset structure values
    Vex___024root___ctor_var_reset(this);
}

void Vex___024root::__Vconfigure(Vex__Syms* _vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->vlSymsp = _vlSymsp;
}

Vex___024root::~Vex___024root() {
}
