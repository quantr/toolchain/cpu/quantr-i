ori     t4,t4, 0x1
ori     t4,t4, 0x11

addi	x3,	x0,	1
sb		x4, x3, 4

; lb
auipc	x7, 0xfc10


addiw	t3,zero,-1
slli    t3,t3,0x26
addi    t3,t3,65
slli    t3,t3,0xd
addi    t3,t3,837
slli    t3,t3,0xc
addi    t3,t3,1656
srai    t2,t3,0x2

addi	t2, t2, 0x10
addi	t2, t2, 0x20
addi	t2, t2, 0x30
addi    t2, t2, -0x10
addi    t2, t2, -0x20
addi    t2, t2, -0x30

addi	t1, x0, 0x22
addi	t2, x0, 0x234

or      t3,t2, t1
ori     t4,t3, 0x15

addi	t1, x0, 0x22
addi	t2, x0, 0x234

xor		t3,t2, t1

addi	t1, x0, 0x22
xori	t4,t1, 0x22

addi	t1, x0, 0x22
addi	t2, x0, 0x234

and		t3,t2, t1
andi	t4,t3, 0x80

addi	t1, x0, 0x22
addi	t2, x0, 0x234

slt		t3,t2, t1
sltu	t4,t3, t1

addi	t1, x0, 0x22
addi	t2, x0, 0x234

srl		t4,t3, t1
sra		t4,t3, t1

addi	t1, x0, 0x22
addi	t2, x0, 0x234

sll		t4,t3, t1

addi	t1, x0, 0x22
addi	t2, x0, 0x234

;li		t3, 0x82345678
lui		x28,0xfff82345
addi	x28,x28,0x678
srai	t2, t3, 0x2
srli	t2, t3, 0x12
slli	t2, t3, 0x12

lui		t2,0x1000

ld      t2,t3, 0x12
sd      t2, t3, 0x32

addi t2,t2, 0x12
addi t1,t1, 0x12
beq t2, t1, 0x1234


addi t2,t2, 0x12
addi t1,t1, 0x13
bne t2, t1, 0x1234

addi t2,t2, 0x12
addi t1,t1, 0x13
blt t2, t1, 0x1234
 
addi t2,t2, 0x12
addi t1,t1, 0x13
bgt t2, t1, 0x1234

addi t2,t2, 0x12
addi t1,t1, 0x13
bltu t2, t1, 0x1234

addi t2,t2, 0x12
addi t1,t1, 0x13
bgeu t2, t1, 0x1234

lb x1, x2, 0x12

lh x3, x1, 0x34

lw x3, x1, 0x34

lbu x3, x1, 0x34

lhu x3, x1, 0x34