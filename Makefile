wsl=$(grep Microsoft /proc/version)
ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
	detected_OS := Windows
	UISCALE := 1
else ifneq ($(wsl),)
	detected_OS := WSL
	UISCALE := 1
else
	detected_OS := $(shell uname)  # same as "uname -s"
	UISCALE := 2
endif
	
all: rom
	#verilator --coverage-line --prof-cfuncs --trace --trace-coverage -Wall --cc --exe --build src/sim_main.cpp src/quantr_i_main.v -Isrc
	verilator --trace --trace-coverage -Wall --cc --exe --build src/sim_main.cpp src/quantr_i_main.v -Isrc

all2: rom
	mkdir src-output
	cp src/*.cpp src-output
	cp src/*.h src-output
	# java -jar jar/verilog-compiler-*.jar -t src -tvh '`include "quantr_dpi.h"' -o src-output
	java -jar jar/verilog-compiler-*.jar -t src -tvh '' -o src-output
	verilator --trace -Wall --coverage --cc --exe --build src-output/sim_main.cpp src-output/quantr_i_main.v -Isrc-output -debug --assert
	#verilator --trace -Wall --coverage --cc quantr_i_main.v --exe sim_main.cpp -Isrc-output # -debug --assert
	make OPT_FAST="-O2 -fno-stack-protector" -j -C obj_dir -f Vquantr_i_main.mk Vquantr_i_main

rom:
	java -jar jar/assembler-*.jar -a rv64 rom2.s -l rom2.lst -o rom.out
	cat rom.out|od -t x4 -A x4|sed 's/^.......//'| sed 's/ //g'|tr -d '\n'|sed 's/......../&\n/g' > rom.data
	rm rom.out

clean:
	-rm -fr obj_dir
	-rm -fr src-output
	-rm -fr temp
	-rm -fr output.vcd
	-rm -fr output.svg
	-rm -fr yosys_show.dot
	-rm -fr rom.data
	-rm -fr coverage
	-rm -fr gmon.out
	-rm -fr coverage.txt
	-rm -fr rom1.lst
	-rm -fr rom2.lst
	-rm -fr quantr.profiling

run:
	./obj_dir/Vquantr_i_main
	@#curl -X POST -H "Content-Type: multipart/form-data" -F "file1=@output.vcd" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadVCDFile/peter@quantr.hk/1234/test1
	@#java -jar verilogcompiler-1.0.jar -p .,quantr_i.v > topmodule.json
	@#curl -X POST -H "Content-Type: multipart/form-data" -F "file1=@topmodule.json" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadTopmoduleJson/peter@quantr.hk/1234/test1
	@#rm -fr topmodule.json
	@#curl -X POST https://www.quantr.foundation/wp-json/wordpress-vcd/uploadScopeSequence/peter@quantr.hk/1234/test1/pc_reg,rom,if_id,id,id_ex,ex,ex_mem,mem,mem_wb

yosys:
	cat synthesis.yo | yosys
	cat sys.yo | yosys
	cp ~/.yosys_show.dot yosys_show.dot
	dot -Tsvg yosys_show.dot -o output.svg
	curl -X POST -H "Content-Type: multipart/form-data" -F "file1=@output.svg" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadYosys/peter@quantr.hk/1234/test1
	curl -X POST -H "Content-Type: multipart/form-data" -F "file1=@output.vcd" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadVcd/peter@quantr.hk/1234/test1
	curl -X POST https://www.quantr.foundation/wp-json/wordpress-vcd/uploadScopeSequence/peter@quantr.hk/1234/test1/pc_reg
	./uploadAllVerilogFiles.sh
	rm output.svg

vcd: all run
	#java -Dsun.java2d.uiScale=$(UISCALE) -jar jar/quantr-vcd-component*.jar -f output.vcd &
	java -jar jar/quantr-vcd-component*.jar -f output.vcd &

graph:
	java -Dsun.java2d.uiScale=$(UISCALE) -jar jar/verilog-graph-2.0.jar src/quantr_i.v

coverage_report:
	verilator_coverage  -write-info coverage.info coverage.txt
	genhtml coverage.info --output-directory coverage

